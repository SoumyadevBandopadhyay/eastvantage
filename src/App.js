import './App.css';
import RandomUser from './components/RandomUser';
import useAxios from './customHooks/useAxios';
import {useState} from "react"

function App() {

  const [refresh, setRefresh]= useState(false)
  const {
    email,
    fullName,
    isPending,
    error
  } = useAxios("https://randomuser.me/api", refresh)


  const handleRefresh =()=>{
    setRefresh(!refresh)
  }

  return (
    <div className="App">
      { error && <div>{ error }</div> }
      { isPending && <div>Loading...</div> }
      {email && fullName &&<RandomUser email={email} fullName={fullName} handleRefresh={handleRefresh}/>}
    </div>
  );
}

export default App;
