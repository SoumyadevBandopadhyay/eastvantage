

const RandomUser = ({handleRefresh, email, fullName}) =>{
    return (
        <div style={{display:"flex", flexDirection:"column", justifyContent:"center"}}>
            <div>
                <p><span style={{fontWeight:"bolder"}}>Name: </span><span style={{color:"green"}}>{fullName}</span></p>
            </div>
            <div>
                <p><span style={{fontWeight:"bolder"}}>Email Id: </span><span style={{color:"blue"}}>{email}</span></p>
            </div>
            <div>
                <button onClick={handleRefresh}>Refresh</button>    
            </div>
        </div>
    )
}

export default RandomUser