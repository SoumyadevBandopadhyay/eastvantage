import axios from "axios"
import { useState, useEffect } from "react"

const useAxios = (url, refresh) => {
    const [fullName, setFullName]=useState("")
    const [email, setEmail]=useState("")
    const [isPending, setIsPending] = useState(true);
    const [error, setError] = useState(null);


    useEffect(() => {
        async function fetchData() {
            try {
                const { status, data: { results: data } } = await axios.get(url)
                const { title, first, last } = data[0].name
                const { email } = data[0]
                const fullName = title + " " + first + " " + last
                setIsPending(false)
                setError(null)
                setEmail(email)
                setFullName(fullName)
                localStorage.setItem("data", JSON.stringify(data))
                if (status !== 200) {
                    throw Error("Couldn't fetch the requested resource")
                }
                // console.log(fullName, email, status)
            } catch (err) {
                setIsPending(false);
                setError(err.message);
                // console.log(err.message)
            }
        }



        fetchData()
    }, [url, refresh])

    return {
        email,
        fullName,
        isPending,
        error
    }
}

export default useAxios